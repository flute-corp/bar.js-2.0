import Vue from 'vue'
import axios from 'axios'
// import { Cookies } from 'quasar'

const axiosInstance = axios.create({
  baseURL: process.env.API_URL + '/api/',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }
  // transformRequest: [
  //   (data, headers) => {
  //     const token = Cookies.get('token')
  //     if (token) {
  //       headers['X-AUTH-TOKEN'] = token
  //     }
  //     return JSON.stringify(data)
  //   }
  // ]
})

Vue.prototype.$axios = axiosInstance

export { axiosInstance }
