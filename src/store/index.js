import Vue from 'vue'
import Vuex from 'vuex'

import items from './items'
import panier from './panier'
import tech from './tech'
import localStoragePlugin from './plugins/localStoragePlugin'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      items,
      panier,
      tech
    },
    plugins: [localStoragePlugin()],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  if (process.env.DEV && module.hot) {
    module.hot.accept(['./items', './panier', './tech'], () => {
      const newItems = require('./items').default
      const newPanier = require('./panier').default
      const newTech = require('./tech').default
      Store.hotUpdate({
        modules: {
          items: newItems,
          panier: newPanier,
          tech: newTech
        }
      })
    })
  }

  return Store
}
