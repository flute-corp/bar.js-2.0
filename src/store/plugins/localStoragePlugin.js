import { LocalStorage, Notify } from 'quasar'

const ITEMS = 'items'
const PANIER = 'panier'

const localStoragePlugin = function () {
  return store => {
    store.subscribeAction((action, payload) => {
      switch (action.type) {
        case 'panier/restorPanierAction':
          if (LocalStorage.has(PANIER)) {
            const { panier } = LocalStorage.getItem(PANIER)
            if (panier && panier.length > 0) {
              store.commit('panier/setPanier', { panier })
              Notify.create({
                type: 'positive',
                message: 'Restauration du panier automatique'
              })
            }
          }
          break
        case 'items/restorItemsAction':
          if (LocalStorage.has(ITEMS)) {
            const { items } = LocalStorage.getItem(ITEMS)
            store.commit('items/setItems', { items })
          }
          break
      }
    })

    store.subscribe((mutation, state) => {
      switch (mutation.type) {
        case 'items/setItems':
        case 'items/addItem':
        case 'items/removeItem':
          LocalStorage.set(ITEMS, state.items)
          break
        case 'items/dropItems':
          LocalStorage.remove(ITEMS)
          break
        case 'panier/addPanier':
        case 'panier/removeItemPanier':
        case 'panier/updateItemPanier':
        case 'panier/addItemPanier':
          LocalStorage.set(PANIER, state.panier)
          break
        case 'panier/dropPanier':
          LocalStorage.remove(PANIER)
          break
      }
    })
  }
}

export default localStoragePlugin
