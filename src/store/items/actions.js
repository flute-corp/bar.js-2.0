import { axiosInstance } from 'boot/axios'

export async function listItemsAction ({ commit }) {
  const r = await axiosInstance('items')
  const { items } = r.data
  commit('setItems', { items })
  return r.data
}

export function restorItemsAction () {}
