export const getItems = (state) => state.items
export const getItemsCat1 = (state) => state.items.filter((el) => parseInt(el.categorie_id) === 1)
export const getItemsCat2 = (state) => state.items.filter((el) => parseInt(el.categorie_id) === 2)
export const getItemsCat3 = (state) => state.items.filter((el) => parseInt(el.categorie_id) === 3)
export const getItemsCat4 = (state) => state.items.filter((el) => parseInt(el.categorie_id) === 4)
export const getItemsCat5 = (state) => state.items.filter((el) => parseInt(el.categorie_id) === 5)
