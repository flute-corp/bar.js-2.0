export const addItem = (state, { item }) => {
  state.items.push(item)
}
export const setItems = (state, { items }) => {
  state.items = items
}
export const removeItem = (state, { item }) => {
  const items = state.items
  const index = items.indexOf(items.find((el) => item.id === el.id))
  items.splice(index, 1)
}
export const dropItems = (state) => {
  state.items = []
}
export default function () {
  return {
    items: true
  }
}
