import { Cookies } from 'quasar'

export function setConnectedAction ({ commit }, payload) {
  commit('setConnected', payload)
}

export function setUsernameAction ({ commit }) {
  if (Cookies.has('username')) {
    const username = Cookies.get('username')
    commit('setUsername', { username })
  }
}

export function setTokenAction ({ commit }) {
  if (Cookies.has('token')) {
    const token = Cookies.get('token')
    commit('setToken', { token })
  }
}
