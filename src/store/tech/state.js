/**
 * @returns {{tech: {online: boolean, username: string, token: string}}}
 */
export default function () {
  return {
    tech: {
      online: navigator.onLine,
      username: '',
      token: ''
    }
  }
}
