export const getTech = (store) => store.tech
export const online = (store) => store.tech.online
export const getUsername = (store) => store.tech.username
export const getToken = (store) => store.tech.token
