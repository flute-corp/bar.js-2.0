export const setConnected = (state, { online }) => {
  state.tech.online = online
}

export const setUsername = (state, { username }) => {
  state.tech.username = username
}

export const setToken = (state, { token }) => {
  state.tech.token = token
}
