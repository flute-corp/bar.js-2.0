export function insertItemPanierAction ({ commit, state }, { item }) {
  commit('addItemPanier', { item })
}

export function updateItemPanierAction ({ commit, state }, { itemPanier }) {
  commit('updateItemPanier', { itemPanier })
}

export function removeItemPanierAction ({ commit }, { itemPanier }) {
  commit('removeItemPanier', { itemPanier })
}

export function dropPanierAction ({ commit }) {
  commit('dropPanier')
}

export function addPanierAction ({ commit, state }, { panier }) {
  commit('addPanier', { panier })
}

export function restorPanierAction () {}
