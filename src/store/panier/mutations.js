let GENERATE_ID = 0

export const addItemPanier = (state, { item }) => {
  const itemPanier = {
    id: ++GENERATE_ID,
    quantite: 1,
    label: item.label,
    idItem: parseInt(item.id),
    prix: item.prix
  }
  state.panier.push(itemPanier)
}

export const updateItemPanier = (state, { itemPanier }) => {
  ++itemPanier.quantite
}

export const removeItemPanier = (state, { itemPanier }) => {
  const itemsPanier = state.panier
  if (itemPanier.quantite === 1) {
    const index = itemsPanier.indexOf(itemPanier)
    itemsPanier.splice(index, 1) // Suppression de l'élément du panier
  } else {
    --itemPanier.quantite
  }
}

export const dropPanier = (state) => {
  state.panier = []
}

export const setPanier = (state, { panier }) => {
  state.panier = panier
}

export default function () {
  return {
    panier: true
  }
}
