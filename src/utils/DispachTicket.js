class DispachTicket {
  constructor () {
    this._nCard = 1
    this._panier = []
    this._id = 0
  }

  /**
   * @param panier {[]}
   */
  set panier (panier) {
    this._panier = [...panier]
  }

  /**
   * @param nCard {number}
   */
  set nCard (nCard) {
    this._nCard = nCard
  }

  /**
   *
   * @param aTableau {[{}]}
   * @param key {string}
   * @param order {string} asc || desc
   * @returns {[]}
   */
  sortKey (aTableau, key, order) {
    order = order || 'asc'
    if (order !== 'desc' && order !== 'asc') {
      console.error('Paramètre invalide (order)')
      throw new Error('Paramètre invalide (order)')
    }
    return aTableau.sort((a, b) => {
      if (a[key] < b[key]) return order === 'desc' ? 1 : -1
      if (a[key] > b[key]) return order === 'desc' ? -1 : 1
      return 0
    })
  }

  /**
   * @param aTableau {[]}
   * @param key {string}
   * @returns {*}
   */
  reduceKey (aTableau, key) {
    return aTableau.reduce((a, b) => {
      return a[key] <= b[key] ? a : b
    })
  }

  /**
   * @param aTableau {[]}
   * @returns {number}
   */
  sumArray (aTableau) {
    return aTableau.reduce((a, b) => a + b)
  }

  getCardSumMin (aCard) {
    const aCardSum = []
    for (const card of aCard) {
      const oCard = {
        card: card,
        sum: 0
      }
      if (card.length > 0) {
        oCard.sum = this.sumArray(card.map((el) => Math.round((el.prix * el.quantite) * 100))) / 100
      }
      aCardSum.push(oCard)
    }
    return this.reduceKey(aCardSum, 'sum').card
  }

  run () {
    const listItemsCard = []
    for (let i = 0; i < this._nCard; ++i) {
      listItemsCard[i] = []
    }
    // Un item par prix
    const listItems = []
    for (const item of this._panier) {
      for (let i = 0; i < item.quantite; ++i) {
        listItems.push({
          id: ++this._id,
          prix: item.prix,
          quantite: 1,
          label: item.label,
          idItem: item.idItem
        })
      }
    }
    // 1 / Tri décroissant des articles par prix
    const listItemsSort = this.sortKey(listItems, 'prix', 'desc')
    // 2 / Distribution récursive sur la carte la moins chargée
    for (const item of listItemsSort) {
      const card = this.getCardSumMin(listItemsCard)
      const itemCard = card.find((el) => el.idItem === item.idItem)
      if (itemCard) {
        ++itemCard.quantite
      } else {
        card.push(item)
      }
    }
    return listItemsCard
  }
}

export default DispachTicket
